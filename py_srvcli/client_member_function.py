import sys

from example_interfaces.srv import AddTwoInts
from trajectory_service_interface.srv import Traj
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64

import rclpy
from rclpy.node import Node




class MinimalClientAsync(Node):

    def __init__(self):
        super().__init__('minimal_client_async')
        self.cli = self.create_client(Traj, 'waypoint')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = Traj.Request()

    def send_request(self):
        self.req.time = Float64(sys.argv[0])
        self.future = self.cli.call_async(self.req)


def main(args=None):
    rclpy.init(args=args)

    minimal_client = MinimalClientAsync()
    minimal_client.send_request()

    while rclpy.ok():
        rclpy.spin_once(minimal_client)
        if minimal_client.future.done():
            try:
                response = minimal_client.future.result()
            except Exception as e:
                minimal_client.get_logger().info(
                    'Service call failed %r' % (e,))
            else:
                minimal_client.get_logger().info('Result for x:')
                minimal_client.get_logger().info(response.pose.x)
            break

    minimal_client.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()