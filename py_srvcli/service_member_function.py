from example_interfaces.srv import AddTwoInts
from trajectory_service_interface.srv import Traj
from geometry_msgs.msg import Pose

import pandas as pd
import numpy as np

import rclpy
from rclpy.node import Node


class TrajectoryService(Node):


    def __init__(self):
        super().__init__('trajectory_service')
        self.srv = self.create_service(Traj, 'waypoint', self.waypoint_callback)
        self.df = pd.read_csv('/home/elie/colcon_ws/src/py_srvcli/py_srvcli/traj.csv',delimiter=',',index_col=False)

    def euler_to_quaternion(roll, pitch, yaw):

        qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
        qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
        qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

        return [qx, qy, qz, qw]


    def waypoint_callback(self, request, response):
        
        self.get_logger().info('request is: ')
        self.get_logger().info(request.time)

        # adding new row to the dataframe
        new_row = {'t': request.time, 'x': np.nan, 'y': np.nan, 'z': np.nan, 'psi': np.nan}
        self.df = self.df.append(new_row, ignore_index=True)
        self.df.sort_values('t')
        self.df = self.df.reset_index(drop = True)

        # interpolation
        self.get_logger().info('interpolating... ')
        self.df = self.df.interpolate(method='polynomial', order=7)
        self.get_logger().info('interpolation done')

        self.get_logger().info('fetching the interpolated row')
        # retreiveing the interpolated row
        row = self.df.loc[ self.df['t'] == request.time ]

        self.get_logger().info('finding the quaternion')
        q = self.euler_to_quaternion(0.0,0.0,row[3])
        
        self.get_logger().info('Storing the response')
        response.pose.position.x = row[0]
        response.pose.position.x = row[1]
        response.pose.position.x = row[2]

        response.pose.orientation.x = q[0]
        response.pose.orientation.y = q[1]
        response.pose.orientation.z = q[2]
        response.pose.orientation.w = q[3]

        self.get_logger().info('request received: ')
        self.get_logger().info(request.time)

        return response


def main(args=None):
    rclpy.init(args=args)

    trajectory_service = TrajectoryService()

    rclpy.spin(trajectory_service)

    rclpy.shutdown()


if __name__ == '__main__':
    main()